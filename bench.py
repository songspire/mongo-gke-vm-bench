#!/usr/bin/env python3
import os
import subprocess
import re
import csv
from pymongo import MongoClient
from collections import OrderedDict

YCSB_HOME = os.getenv('YCSB_HOME', "/usr/local/lib/ycsb-0.17.0")

output_filename = "results.csv"

def run(workload='a', num_threads=8, num_records=1000, num_operations=1000, ycsb_home='', mongodb=''):
    result = subprocess.run(["/usr/local/bin/ycsb", 
        "run", 
        "mongodb-async", 
        "-s", 
        "-threads {}".format(num_threads), 
        "-P {}/workloads/workload{}".format(ycsb_home, workload), 
        "-p mongodb.url=mongodb://{}".format(mongodb), 
        "-p recordscount={}".format(num_records), 
        "-p operationcount={}".format(num_operations)], 
        stdout=subprocess.PIPE) 
        #stderr=subprocess.STDOUT)

    mongo = MongoClient(mongodb)
    mongo.drop_database('ycsb')
    mongo.close()

    return(result.stdout.decode('utf-8'))


if __name__ == '__main__':

    num_runs = 3
    csvfile = open(output_filename, 'w')
    writer = None

    #writer = csv.DictWriter(csvfile, fieldnames=record.keys())

    #record['runtime'] = re.search(r'^\[OVERALL\], RunTime\(ms\), (.+)$', output, re.MULTILINE).group(1)
    #record['throughput'] = re.search(r'^\[OVERALL\], Throughput\(ops\/sec\), (.+)$', output, re.MULTILINE).group(1)

    num_records = 10000
    num_operations = 10000
    host_address = {'k8s': '10.156.0.15:30400'} 
    for hostname in host_address:
        for num_threads in [4, 8, 16, 32]:

            for n in range(num_runs):
                record = OrderedDict()

                record['host'] = hostname
                record['num_threads'] = num_threads
                record['num_records'] = num_records
                record['num_operations'] = num_operations

                output = run(workload='d', 
                    num_threads = num_threads, 
                    num_records = num_records, 
                    num_operations = num_operations, 
                    ycsb_home = YCSB_HOME, 
                    mongodb = host_address[hostname]
                )

                results = re.finditer(r'\[(\w+)\], (.+), (.+)', output)
                for r in results:
                    #print("{}: {}".format(r.group(1)+'_'+r.group(2).split('(')[0], r.group(3)))
                    #print("{}: {}".format(r.group(1)+'_'+r.group(2), r.group(3)))
                    record[r.group(1)+'_'+r.group(2)]=r.group(3)

                #print(record)
                if writer == None:
                    writer = csv.DictWriter(csvfile, fieldnames=record.keys())
                    writer.writeheader()

                writer.writerow(record)


    csvfile.close()

    quit()
