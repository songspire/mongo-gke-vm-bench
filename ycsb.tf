variable "benchmark_host_name" {
  type = string
  default = "ycsb2"
  description = "The hostname of the instance that will be running ycsb"
}

resource "google_compute_instance" "ycsb" {
    allow_stopping_for_update = true
    name = var.benchmark_host_name
    machine_type = "e2-custom-8-4096"
    zone = var.zone

    tags = ["ycsb"]

    boot_disk {
        initialize_params {
            image = "debian-cloud/debian-9"
        }
    }

    network_interface {
        network = var.network

        access_config {
        }
    }

    metadata_startup_script = file("ycsb_setup.sh")

    service_account {
        scopes = ["compute-ro", "storage-rw"]
    }
}