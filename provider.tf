
variable "project_id" {
    type = string
    default = "tmp1-271812"
}

variable "region" {
    type = string
    default = "europe-west3"
}

variable "zone" {
    type = string
    default = "europe-west3-a"
}

variable "network" {
    type = string
    default = "default"
}

provider "google" {
  project = var.project_id
  region  = var.region
  zone    = var.zone
}