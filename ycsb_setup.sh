#!/bin/bash

MONGO_VERSION=4.2.8

sudo apt-get update
sudo apt-get -y upgrade

sudo apt install -y default-jdk

cd /tmp

curl -O --location https://github.com/brianfrankcooper/YCSB/releases/download/0.17.0/ycsb-0.17.0.tar.gz

tar xfz ycsb-0.17.0.tar.gz -C /usr/local/lib && rm ycsb-0.17.0.tar.gz

chmod -R 755 /usr/local/lib/ycsb-0.17.0/

sed '/# Only set YCSB_HOME if not already set/i YCSB_HOME=/usr/local/lib/ycsb-0.17.0' /usr/local/lib/ycsb-0.17.0/bin/ycsb.sh > /usr/local/bin/ycsb

chmod 755 /usr/local/bin/ycsb

#ln -s /usr/local/lib/ycsb-0.17.0/bin/ycsb /usr/local/bin/ycsb

# Install mongoDB community edition
wget -qO - https://www.mongodb.org/static/pgp/server-4.2.asc | sudo apt-key add -

echo "deb http://repo.mongodb.org/apt/debian stretch/mongodb-org/4.2 main" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.2.list

sudo apt-get update

sudo apt-get install -y mongodb-org=$MONGO_VERSION mongodb-org-server=$MONGO_VERSION mongodb-org-shell=$MONGO_VERSION mongodb-org-mongos=$MONGO_VERSION mongodb-org-tools=$MONGO_VERSION

sudo apt-get install -y python3-pymongo

curl -LO https://gitlab.com/songspire/mongo-gke-vm-bench/-/raw/master/bench.py

mv bench.py /usr/local/bin/

chmod 755 /usr/local/bin/bench.py