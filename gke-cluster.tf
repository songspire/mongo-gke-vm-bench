variable "cluster_name" {
    type = string
    default = "mongo-bench"
}

resource "google_container_cluster" "gke-cluster" {
  name     = var.cluster_name
  location = var.zone

  # We can't create a cluster with no node pool defined, but we want to only use
  # separately managed node pools. So we create the smallest possible default
  # node pool and immediately delete it.
  remove_default_node_pool = true
  initial_node_count       = 1

  resource_labels = {
      environment = "test"
      workload = "mongo-bench"
  }

  master_auth {
    username = ""
    password = ""

    client_certificate_config {
      issue_client_certificate = false
    }
  }
}

resource "google_container_node_pool" "mongodb" {
  name       = "mongodb"
  location   = var.zone
  cluster    = google_container_cluster.gke-cluster.name
  node_count = 1

  node_config {
    machine_type = "e2-custom-10-32768"
    image_type = "ubuntu"
    metadata = {
      disable-legacy-endpoints = "true"
    }

    labels = {
      environment = "test",
      workload = "mongo-bench"
    }

    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]
  }
}
